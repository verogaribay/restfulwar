﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestFulWAR.BD;

namespace RestFulWAR.Models
{
    public class Negocio
    {
        List<Carta> lstCartas = new List<Carta>();
        List<Score> lstScore = new List<Score>();
        Persistencia objP = new Persistencia();

        public List<Carta> ObtenerCartas()
        {
            lstCartas = objP.ObtenerCartas();

            return lstCartas;
        }

        public List<Score> ObtenerScore()
        {
            lstScore = objP.ObtenerScore();

            return lstScore;
        }

        public void InsertarScore(int IdJugador)
        {
            objP.InsertarScore(IdJugador);
        }
    }
}
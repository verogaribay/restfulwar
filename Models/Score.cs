﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestFulWAR.Models
{
    public class Score
    {
        public int IdJugador { get; set; }

        public int Wins { get; set; }

        public string dtFecha { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestFulWAR.Models
{
    public class Carta
    {
        public int IdJugador { get; set; }
        public int IdCarta { get; set; }
        public string NoCarta { get; set; }
        public int ValorCarta { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestFulWAR.Models;

namespace RestFulWAR.BD
{
    public class Persistencia
    {
        List<Carta> lstCartas = new List<Carta>();
        List<Score> lstScore = new List<Score>();

        public List<Carta> ObtenerCartas()
        {
            lstCartas = new List<Carta>();

            using (BD_AspenCapitalEntities BD = new BD_AspenCapitalEntities())
            {
                lstCartas = (from CC in BD.ObtenerCartas()
                             select new Carta
                             {
                                 IdJugador = (int)CC.idJugador,
                                 IdCarta = CC.idCarta,
                                 NoCarta = CC.NoCarta,
                                 ValorCarta = CC.ValorCarta
                             }).ToList();
            }
            return lstCartas;
        }

        public List<Score> ObtenerScore()
        {
            lstScore = new List<Score>();

            using (BD_AspenCapitalEntities BD = new BD_AspenCapitalEntities())
            {
                lstScore = (from CC in BD.ObtenerScore2()
                            select new Score
                            {
                                IdJugador = (int)CC.idJugador,
                                Wins = (int)CC.WINS,
                                dtFecha = Convert.ToString(CC.dtFecha)
                            }).ToList();
            }
            return lstScore;
        }

        public bool InsertarScore(int IdJugador)
        {
            bool bExito = new bool();

            try
            {
                using (BD_AspenCapitalEntities BD = new BD_AspenCapitalEntities())
                {
                    tbScore obj = new tbScore();
                    obj.idJugador = IdJugador;
                    obj.dtFecha = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time"));

                    BD.tbScores.Add(obj);
                    BD.SaveChanges();
                    bExito = true;
                }
            }
            catch (Exception ex)
            {
                bExito = false;
            }
            return bExito;
        }
    }
}
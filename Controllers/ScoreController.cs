﻿using RestFulWAR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestFulWAR.Controllers
{
    public class ScoreController : ApiController
    {
        List<Score> lstScore = new List<Score>();
        Negocio objNegocio = new Negocio();

        public ScoreController()
        {
            lstScore = objNegocio.ObtenerScore();
        }

        // GET api/<controller>
        public List<Score> GetScore()
        {
            return lstScore;
        }

        [HttpPost]
        // POST api/<controller>
        public void Post([FromBody] Carta s)
        {
            objNegocio.InsertarScore(s.IdJugador);
        }

    }
}
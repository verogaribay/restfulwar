﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RestFulWAR.Models;

namespace RestFulWAR.Controllers
{
    public class CartasController : ApiController
    {
        List<Carta> lstCartas = new List<Carta>();
        Negocio objNegocio = new Negocio();

        public CartasController()
        {
            lstCartas = objNegocio.ObtenerCartas();
        }

        // GET api/<controller>
        public List<Carta> GetCartas()
        {
            return lstCartas;
        }
    }
}